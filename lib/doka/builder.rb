require 'fileutils'
require 'multi_json'

module Doka
  class Builder

    def initialize(path)
      @path = path
      @options = {}
      if File.exist? File.join(path, 'doka.json')
        File.open(File.join(path, 'doka.json'), 'r') do |f|
          @options = MultiJson.load(f.read)
        end
      end
    end

    def index
      index ||= Doka::Tree::Index.new(@path, @options)
    end

    def build
      puts "Build started in #{build_path}"

      FileUtils.rm_rf(build_path) if File.exists? build_path
      Dir.mkdir(build_path)

      root  = index.recurse

      menu = Doka::Menu.new(root).build

      build_item(root, menu)
      build_assets
    end

    def build_item(root, menu)
      root.items.each do |item|
        if item.respond_to? :items
          build_item(item, menu)
        else
          # specific file
          local_path = File.join(build_path, item.index.local_url(item))
          local_dir  = File::dirname(local_path)

          FileUtils.mkdir_p(local_dir) unless File.directory? local_dir

          doc = load_asciidoc(item.path)

          File.open(local_path, 'w') do |f|
            f.write render_asciidoc(doc, menu)
          end
        end
      end
    end

    def build_assets
      if @options.has_key? 'assets'
        @options['assets'].each do |asset|
          FileUtils.cp_r File.join(@path, asset), build_path
        end
      end
    end

    def build_path
      File.join(@path, '.build')
    end

    def load_asciidoc(file)

      if @options.key? 'asciidoctor'
        Asciidoctor.load(File.new(file), @options['asciidoctor'].inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo})
      else
        Asciidoctor.load(File.new(file),
                         'source-highlighter'.to_sym => 'coderay',
                         :header_footer => false)
      end
    end

    def render_asciidoc(doc, menu)

      title = doc.doctitle

      html = '<!doctype html>' +
          '<html>' +
          '<head>' +
          get_stylesheets +
          get_javascripts +
          '<title>' + (title.nil? ? '' : title) + '</title>' +
          '</head>' +
          '<body>' +
          '<div id="menu">' + menu + '</div>'

      unless title.nil?
        html += '<div id="header">' +
            '<h1>' + title + '</h1>' +
            '</div>'
      end

      html += '<div id="content">' +
          doc.render +
          '</div>' +
#          '<div id="footer"></div>'+
          '</body>' +
          '</html>'
    end

    def get_stylesheets
      styles = []
      if @options.has_key? 'stylesheets'
        @options['stylesheets'].each do |style|
          styles << "<link rel='stylesheet' type='text/css' href='#{style}'>"
        end
      end
      styles.join $\
    end

    def get_javascripts
      scripts = []
      if @options.has_key? 'javascripts'
        @options['javascripts'].each do |script|
          scripts << "<script src='#{script}'></script>"
        end
      end
      scripts.join $\
    end

  end
end