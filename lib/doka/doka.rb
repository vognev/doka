require 'asciidoctor'
require 'listen'
require 'fileutils'

module Doka

  class << self

    def build(path)
      throw "#{path} - not a directory" unless Dir.exist? path
      builder = Doka::Builder.new path
      builder.build
    end

    def serve(path)
      throw "#{path} - not a directory" unless Dir.exist? path

      builder = Doka::Builder.new path
      builder.build

      @server = Doka::Server.new builder.build_path

      listener = Listen.to(path, :debug => true) do |m, a, r|
        changed = false
        m.each { |i| changed = true if i['.build'].nil? }
        a.each { |i| changed = true if i['.build'].nil? }
        r.each { |i| changed = true if i['.build'].nil? }
        builder.build if changed
      end

      listener.start

      trap('INT') do
        listener.stop
        @server.stop
      end

      @server.start

    end

  end

end