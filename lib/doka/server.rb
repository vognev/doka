require 'webrick'

module Doka
  class Server
    def initialize(build)
      @build = build
    end

    def start
      @server = WEBrick::HTTPServer.new :Port => 3000,
                                        :DocumentRoot => File.join(@build),
                                        :WEBrickThread => true
      @server.start
    end

    def stop
      @server.shutdown
    end

  end
end