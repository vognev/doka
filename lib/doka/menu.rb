module Doka
  class Menu
    def initialize(tree)
      @tree = tree
    end

    def build(node = nil)

      node = @tree if node.nil?
      chunks = []

      chunks.push('<ul>')

      node.items.each do |item|
        url = node.index.local_url(item)
        if item.respond_to? :items
          break unless item.items.length
          chunks.push('<li>')
          # link to directory
          chunks.push("<a href=\"#{url}\">#{dir_title(item)}</a>")
          chunks.push build(item) if item.items.length
          chunks.push('</li>')
        else
          chunks.push("<li><a href=\"#{url}\">#{item.title}</a></li>") unless item.name == 'index.html'
        end
      end

      chunks.push('</ul>')
      chunks.join
    end

    def dir_title(item)
      title = item.name.split('_', 2).last
      item.items.each do |child|
        if child.name == 'index.html'
          title = child.title
          break
        end
      end
      title
    end

  end
end