require 'asciidoctor'

module Doka
  module Tree
    class File

      attr_reader :title
      attr_reader :path
      attr_reader :name
      attr_reader :index

      def initialize(path, index)
        @path = path
        doc = Asciidoctor::load_file(path, :header_footer => false)
        @title = doc.doctitle
        @name = ::File::basename(path, '.*') + '.html'
        @index = index
      end

    end
  end
end