module Doka
  module Tree
    class Dir

      attr_reader :title
      attr_reader :items
      attr_reader :path
      attr_reader :name
      attr_reader :index

      def initialize(path, index)
        @path = path
        @name = ::File.basename(path)
        @items = []
        @index = index

        ::Dir.entries(path).sort.each do |entry|
          next if entry == '.'
          next if entry == '..'
          next if entry[0] == '.'
          next if entry[0] == '_'
          next if entry == '.build'

          item = ::File.join(path, entry)

          next if index.exclude?(item)

          if ::File.file?(item) and index.accept?(item)
            @items.push File.new(item, @index)
          end

          if ::File.directory?(item)
            @items.push Dir.new(item, @index)
          end

        end

      end

    end
  end
end