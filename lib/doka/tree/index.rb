module Doka
  module Tree
    class Index

      ACCEPT = ['.txt', '.adoc', '.asciidoc', '.asc', '.ad'].freeze

      def initialize(path, options)
        @path = path
        @options = options
        @patterns = []
        if options.key? 'exclude'
          options['exclude'].each do |pattern|
            @patterns << Regexp.compile(pattern)
          end
        end
      end

      def recurse
        Dir.new(@path, self)
      end

      def local_path(item)
        return item.path.gsub(@path, '') if item.respond_to? :path
        item.gsub(@path, '')
      end

      def local_url(item)
        ::File.join(::File.dirname(local_path(item)), item.name)
      end

      def accept?(path)
        ACCEPT.include? ::File.extname(path)
      end

      def exclude?(path)
        @patterns.each do |pattern|
          return true if pattern.match(path)
        end
        false
      end

    end
  end
end