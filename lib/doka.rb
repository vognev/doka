%w[
  doka/version
  doka/tree/index
  doka/tree/dir
  doka/tree/file
  doka/menu
  doka/builder
  doka/server
  doka/doka
].each do |f|
  require_relative f
end