$LOAD_PATH.unshift(File.expand_path('../lib', __FILE__))
require 'doka/version'

Gem::Specification.new do |s|
  s.name        = 'doka'
  s.version     = Doka::VERSION
  s.executables << 'doka'
  s.date        = '2014-01-17'
  s.summary     = 'doka'
  s.description = ''
  s.authors     = ['Vitaliy Ognev']
  s.email       = 'vitaliy.ognev@gmail.com'
  s.files       = `git ls-files`.split("\n")
  s.license     = 'MIT'

  s.add_runtime_dependency 'asciidoctor'
  s.add_runtime_dependency 'rack', '~> 1.5'
  s.add_runtime_dependency 'listen', '~> 2.0'
  s.add_runtime_dependency 'multi_json'

end