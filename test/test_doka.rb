require 'test/unit'
require 'doka'

class DokaTest < Test::Unit::TestCase
  def test_test
    assert_equal "0.0.1",
                 Doka::VERSION
  end
end